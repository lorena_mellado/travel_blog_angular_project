import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  logo: string;
  logoAlt: string;
  home: string;
  contact: string;
  about: string;

  constructor() { 
    this.logo = 'https://anthoncode.com/wp-content/uploads/2019/01/angular-logo-png.png';
    this.logoAlt = 'Logo de Angular';
    this.home = 'Home';
    this.contact = 'Contact';
    this.about = 'About';
  }

  ngOnInit(): void {
  }

}
