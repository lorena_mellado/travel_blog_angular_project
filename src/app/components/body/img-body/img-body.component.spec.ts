import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImgBodyComponent } from './img-body.component';

describe('ImgBodyComponent', () => {
  let component: ImgBodyComponent;
  let fixture: ComponentFixture<ImgBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImgBodyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImgBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
