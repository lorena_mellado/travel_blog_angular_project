export interface InterfaceBody {
    author: string;
    date: string;
    mins: number;
    category: string;
    title: string;
    image: Image;
    body: string;
}

export interface Image{
    url: string;
    alt: string;
}

