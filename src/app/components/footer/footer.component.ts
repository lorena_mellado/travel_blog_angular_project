import { Component, OnInit } from '@angular/core';

import { Footer } from './interface-footer';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  footer: any | Footer = {};
  resources: string[];
  help: string[];
  community: string[];
  languages: string[];

  constructor() { 
    console.log('Constructor')
    this.resources = ['About', 'Resource Listing', 'Press Kit', 'Blog', 'Usage', 'Analytics'];
    this.help = ['Discord', 'Gitter', 'StackOverflow'];
    this.community = ['Events', 'Meetups', 'Twitter'];
    this.languages = ['English', 'Spanish', 'German'];

    console.log(this.footer);
    console.log(this.resources, this.help, this.community, this.languages);

  }

  ngOnInit(): void {
    console.log('ONINIT')
    this.footer = {
      resources: this.resources,
      help: this.help,
      community: this.community,
      languages: this.languages,
    };
    console.log(this.footer)
  }

}
