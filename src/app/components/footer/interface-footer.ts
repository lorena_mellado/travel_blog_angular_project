
export interface Footer{
    resources: string[];
    help: string[];
    community: string[];
    languages: string[];
}; 

